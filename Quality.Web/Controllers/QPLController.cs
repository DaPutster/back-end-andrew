﻿using System.Collections.Generic;
using System.Web.Http;
using Quality.Web.Models;

namespace Quality.Web.Controllers
{
    [RoutePrefix("api/Qpl")]
    public class QplController : ApiController
    {
        /// <summary>
        /// Returns all QPL records found
        /// </summary>
        [HttpGet]
        [Route("GetQplListItems")]
        public List<QplListItem> GetQplListItems()
        {
            var dataAccess = new QplDataAccess();
            return dataAccess.GetQplListItems(1, int.MaxValue);
        }

        /// <summary>
        /// Returns a limited set of QPL records based on the <paramref name="currentPage"/> and <paramref name="pageSize"/>
        /// values passed in.
        /// </summary>
        [HttpGet]
        [Route("GetQplListItemsPaged/{currentPage}/{pageSize}")]
        public List<QplListItem> GetQplListItemsPaged(int currentPage = 1, int pageSize = 3)
        {
            var dataAccess = new QplDataAccess();
            return dataAccess.GetQplListItems(currentPage, pageSize);
        }
    }
}
