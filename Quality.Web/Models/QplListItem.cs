﻿using System;

namespace Quality.Web.Models
{
    /// <summary>
    /// This class represents a combined data view that is a single row in the 
    /// QPL listing grid
    /// </summary>
    public class QplListItem
    {
        public string Classification { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreatedDateUTC { get; set; }
        public string Customer { get; set; }
        public bool IsCtq { get; set; }
        public string IsCtqYN => IsCtq ? "Yes" : "No";
        public bool IsQualified { get; set; }
        public string IsQualifiedYN => IsQualified ? "Yes" : "No";
        public string Jurisdiction { get; set; }
        public int LastUpdatedById { get; set; }
        public DateTime LastUpdatedDateUTC { get; set; }
        public bool OpenPo { get; set; }
        public string PartName { get; set; }
        public string PartNumber { get; set; }
        public string Revision { get; set; }
        public string RowKey { get; set; }
        public string Supplier { get; set; }
        public string SupplierCodes { get; set; }
        public string ToolDieSetNumber { get; set; }
    }
}