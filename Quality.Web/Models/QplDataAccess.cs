﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Quality.Web.Models
{
    public class QplDataAccess
    {
        /// <summary>
        /// This constant here is for example purposes, but *should* be pulled from a configuration file.
        /// </summary>
        private const string ConnectionString = @"Server=U6046375-TPL-B\SQL2016;Database=NetInspectDemo;Persist Security Info=True;User ID=niuser;Password=nipass;Pooling=True;MultipleActiveResultSets=True;Connect Timeout=60;Encrypt=False;";

        /// <summary>
        /// This is the query used by the GetQplListItems.  This would be better as a stored proceedure, but this is for 
        /// example purposes.
        /// </summary>
        private const string QplQuery = @"
DECLARE @FirstRec		INT
DECLARE @LastRec		INT

SET @FirstRec = ((@CurrentPage - 1) * @PageSize) + 1;
SET @LastRec = (@CurrentPage * @PageSize);

DECLARE @PagingTable TABLE (
	OrderId		INT IDENTITY(1,1) NOT NULL,
	KeyName		NVARCHAR(200)
);

INSERT INTO @PagingTable (KeyName)
SELECT p.PartNumber + '|' + qpl.Revision + '|' + qpl.ToolDieSetNumber + '|' + CAST(qpl.SupplierCompanyId AS NVARCHAR(50))+ '|' + CAST(qpl.CustomerCompanyId AS NVARCHAR(50)) AS [RowKey]	
FROM QualifiedPartsLists qpl
INNER JOIN Parts p
	ON p.PartId = qpl.PartId
INNER JOIN Companies cc 
	ON cc.Id = qpl.CustomerCompanyId
INNER JOIN Companies cs
	ON cs.Id = qpl.SupplierCompanyId
INNER JOIN CustomerSupplierXref csx
	ON csx.CustomerCompanyId = qpl.CustomerCompanyId
	AND csx.SupplierCompanyId = qpl.SupplierCompanyId
ORDER BY p.PartNumber ASC;

SELECT 
	qpl.IsQualified,
	p.PartName,
	p.PartNumber,
	qpl.Revision,
	qpl.ToolDieSetNumber,
	qpl.OpenPo,
	cs.CompanyName AS [Supplier],
	csx.SupplierCodes,
	p.Jurisdiction,
	p.Classification,
	qpl.Ctq AS [IsCtq],
	qpl.CreatedById,
	qpl.CreatedDateUtc,
	qpl.LastUpdatedById,
	qpl.LastUpdatedDateUtc,
	cc.CompanyName AS [Customer],
	p.PartNumber + '|' + qpl.Revision + '|' + qpl.ToolDieSetNumber + '|' + CAST(qpl.SupplierCompanyId AS NVARCHAR(50))+ '|' + CAST(qpl.CustomerCompanyId AS NVARCHAR(50)) AS [RowKey]	

FROM QualifiedPartsLists qpl
INNER JOIN Parts p
  ON p.PartId = qpl.PartId
INNER JOIN Companies cc 
  ON cc.Id = qpl.CustomerCompanyId
INNER JOIN Companies cs
  ON cs.Id = qpl.SupplierCompanyId
INNER JOIN CustomerSupplierXref csx
  ON csx.CustomerCompanyId = qpl.CustomerCompanyId
    AND csx.SupplierCompanyId = qpl.SupplierCompanyId
WHERE 
  p.PartNumber + '|' + qpl.Revision + '|' + qpl.ToolDieSetNumber + '|' + CAST(qpl.SupplierCompanyId AS NVARCHAR(50))+ '|' + CAST(qpl.CustomerCompanyId AS NVARCHAR(50)) 
     IN (SELECT KeyName 
		 FROM @PagingTable
		 WHERE OrderId >= @FirstRec
		   AND OrderId <= @LastRec)
ORDER BY p.PartNumber ASC;";


        /// <summary>
        /// Obtains a list of QPL records from the database in a paged manner.
        /// </summary>
        /// <param name="currentPage">The current page number being viewed when paging</param>
        /// <param name="pageSize">The number of records to return per page.</param>
        /// <returns></returns>
        public List<QplListItem> GetQplListItems(int currentPage = 1, int pageSize = 3)
        {
            var results = new List<QplListItem>();
            using (var conn = new SqlConnection(ConnectionString)) {
                using (var cmd = new SqlCommand(QplQuery, conn) { CommandType = CommandType.Text }) {
                    cmd.Parameters.Add("@CurrentPage", SqlDbType.Int).Value = currentPage;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
                    conn.Open();
                    using (var reader = cmd.ExecuteReader()) {
                        if (reader.HasRows) {
                            while (reader.Read()) {
                                results.Add(new QplListItem {
                                    IsQualified = (bool)reader["IsQualified"],
                                    PartName = reader["PartName"].ToString(),
                                    PartNumber = reader["PartNumber"].ToString(),
                                    Revision = reader["Revision"].ToString(),
                                    ToolDieSetNumber = reader["ToolDieSetNumber"].ToString(),
                                    OpenPo = (bool)reader["OpenPo"],
                                    Supplier = reader["Supplier"].ToString(),
                                    SupplierCodes = reader["SupplierCodes"].ToString(),
                                    Jurisdiction = reader["Jurisdiction"].ToString(),
                                    Classification = reader["Classification"].ToString(),
                                    IsCtq = (bool)reader["IsCtq"],
                                    CreatedById = (int)reader["CreatedById"],
                                    CreatedDateUTC = (DateTime)reader["CreatedDateUtc"],
                                    LastUpdatedById = (int)reader["LastUpdatedById"],
                                    LastUpdatedDateUTC = (DateTime)reader["LastUpdatedDateUTC"],
                                    Customer = reader["Customer"].ToString()
                                });
                            }
                        }
                        reader.Close();
                    }
                }
                conn.Close();
            }
            return results;
        }
    }
}