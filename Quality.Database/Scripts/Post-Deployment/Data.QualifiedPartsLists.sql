﻿
GO

PRINT N'Update default data in [dbo].[QualifiedPartsLists]...';

GO

-- QualifiedPartsLists
;MERGE INTO dbo.QualifiedPartsLists AS Target
USING (
		select 1, 1, 1, 'A', 'H9378-2', 0, 3, 1, 1, '5/4/2017 11:23:24 PM', 1, '5/4/2017 11:23:24 PM', '9/21/2017 11:23:35 PM', 1 union all
		select 2, 0, 1, '', '', 0, 3, 2, 1, '5/9/2017 11:24:32 PM', 1, '5/9/2017 11:24:32 PM', '8/17/2017 11:24:32 PM', 0 union all
		select 3, 1, 5, 'C', 'J88321-A', 1, 3, 2, 4, '6/13/2017 11:25:32 PM', 1, '6/13/2017 11:25:32 PM', NULL, 1 union all
		select 4, 1, 5, 'D', '332AD01', 0, 3, 2, 1, '6/18/2017 11:26:35 PM', 1, '6/18/2017 11:26:35 PM', '8/21/2017 11:26:35 PM', 1 union all
		select 5, 0, 2, '', '3721D-1', 0, 3, 1, 4, '6/20/2017 11:27:29 PM', 1, '6/20/2017 11:27:29 PM', '8/28/2017 11:27:29 PM', 1 union all
		select 6, 1, 3, 'J', '112363-D', 1, 3, 1, 4, '6/28/2017 11:28:28 PM', 1, '6/28/2017 11:28:28 PM', NULL, 1 union all
		select 7, 0, 6, 'A', 'H832D01', 1, 3, 2, 1, '7/18/2017 11:29:20 PM', 1, '7/18/2017 11:29:20 PM', '9/3/2017 11:29:20 PM', 0 union all
		select 8, 1, 6, 'B', 'J7719D-1', 0, 3, 2, 4, '7/9/2017 11:30:05 PM', 1, '7/9/2017 11:30:05 PM', '9/1/2017 11:30:05 PM', 0 union all
		select 9, 1, 6, 'C', 'H832A01', 1, 3, 2, 1, '7/10/2017 11:30:44 PM', 1, '7/10/2017 11:30:44 PM', '9/2/2017 11:30:44 PM', 1 union all
		select 10, 0, 4, 'A', '3422355', 0, 3, 1, 1, '6/30/2017 11:31:43 PM', 1, '6/30/2017 11:31:43 PM', '9/22/2017 11:31:43 PM', 0
) AS Source
(
		Id, 
		IsQualified, 
		PartId, 
		Revision, 
		ToolDieSetNumber, 
		OpenPo, 
		CustomerCompanyId,
		SupplierCompanyId,
		CreatedById, 
		CreatedDateUtc, 
		LastUpdatedById, 
		LastUpdatedDateUtc,
		ExpirationDateUtc,
		Ctq
)
	ON  Target.Id = Source.Id
-- update matched rows
WHEN MATCHED THEN UPDATE 
	SET Target.IsQualified = Source.IsQualified, 
		Target.PartId = Source.PartId, 
		Target.Revision = Source.Revision, 
		Target.ToolDieSetNumber = Source.ToolDieSetNumber, 
		Target.OpenPo = Source.OpenPo, 
		Target.CustomerCompanyId = Source.CustomerCompanyId,
		Target.SupplierCompanyId = Source.SupplierCompanyId,
		Target.CreatedById = Source.CreatedById, 
		Target.CreatedDateUtc = Source.CreatedDateUtc, 
		Target.LastUpdatedById = Source.LastUpdatedById, 
		Target.LastUpdatedDateUtc = Source.LastUpdatedDateUtc,
		Target.ExpirationDateUtc = Source.ExpirationDateUtc,
		Target.Ctq = Source.Ctq
-- insert new rows
WHEN NOT MATCHED BY TARGET THEN	INSERT VALUES 
(
		IsQualified, 
		PartId, 
		Revision, 
		ToolDieSetNumber, 
		OpenPo, 
		CustomerCompanyId,
		SupplierCompanyId,
		CreatedById, 
		CreatedDateUtc, 
		LastUpdatedById, 
		LastUpdatedDateUtc,
		ExpirationDateUtc,
		Ctq
);

GO