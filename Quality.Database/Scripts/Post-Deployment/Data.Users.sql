﻿
GO

PRINT N'Update default data in [dbo].[Users]...';

GO

-- Users
;MERGE dbo.Users AS Target
USING (
		select 1, 3, 'Jim', 'Thompson', 'Jim.Thompson' union all
		select 2, 2, 'Dianne', 'Sandoval', 'Dianne' union all
		select 3, 1, 'Dwight', 'Richards', 'Dwight.Richards' union all
		select 4, 3, 'Jorge', 'Goodman', 'Jorge.Goodman' 
) AS Source (Id, CompanyId, FirstName, LastName, UserName)
	ON Target.Id = Source.Id
-- update matched rows
WHEN MATCHED THEN UPDATE 
	SET Target.CompanyId = Source.CompanyId,
		Target.FirstName = Source.FirstName,
		Target.LastName = Source.LastName,
		Target.UserName = Source.UserName
-- insert new rows
WHEN NOT MATCHED BY TARGET THEN	INSERT 
	VALUES (CompanyId, FirstName, LastName, UserName);

GO